<?php

namespace Drupal\graphql_book\GraphQL\Response;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\GraphQL\Response\Response;

/**
 * Type of response used when a book is returned.
 */
class BookResponse extends Response {

  /**
   * The book to be served.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  protected $book;

  /**
   * Sets the content.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $book
   *   The book to be served.
   */
  public function setBook(?EntityInterface $book): void {
    $this->book = $book;
  }

  /**
   * Gets the book to be served.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The book to be served.
   */
  public function book(): ?EntityInterface {
    return $this->book;
  }

}
