<?php

namespace Drupal\graphql_book\Plugin\GraphQL\DataProducer;

use Drupal\book\BookManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_book\GraphQL\Response\BookResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Creates a new book entity.
 *
 * @DataProducer(
 *   id = "create_book",
 *   name = @Translation("Create Book"),
 *   description = @Translation("Creates a new book."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Book")
 *   ),
 *   consumes = {
 *     "data" = @ContextDefinition("any",
 *       label = @Translation("Book data")
 *     )
 *   }
 * )
 */
class CreateBook extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The book manager service.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected BookManagerInterface $bookManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('book.manager'),
      $container->get('current_user')
    );
  }

  /**
   * CreateBook constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param array $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\book\BookManagerInterface $bookManager
   *   The book manager service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    BookManagerInterface $bookManager,
    AccountInterface $currentUser
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->bookManager = $bookManager;
    $this->currentUser = $currentUser;
  }

  /**
   * Creates a book.
   *
   * @param array $data
   *   The submitted values for the book.
   * 
   * @return \Drupal\graphql_book\GraphQL\Response\BookResponse
   *   The newly created book.
   * 
   * @throws \Exception
   */
  public function resolve(array $data) {
    $response = new BookResponse();

    if (!$this->currentUser->hasPermission("Create new books") || !$this->currentUser->hasPermission("Create new content")) {
      $response->addViolation(
        $this->t('You do not have permissions to create books.')
      );
      return $response;
    }

    $values = [
      'type' => 'book',
      'title' => $data['title'],
      'body' => $data['body'] ?? [
        'value' => '',
        'summary' => ''
      ]
    ];
    $node = $this->entityTypeManager->getStorage('node')->create($values);
    $node->save();

    $data['links'] ??= [];
    $data['links'] += $this->bookManager->getLinkDefaults($node->id());

    $this->bookManager->saveBookLink($data['links'], TRUE);

    $response->setBook($node);
    return $response;
  }

}