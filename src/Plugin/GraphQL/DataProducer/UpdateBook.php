<?php

namespace Drupal\graphql_book\Plugin\GraphQL\DataProducer;

use Drupal\book\BookManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_book\GraphQL\Response\BookResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Update an existing book entity.
 *
 * @DataProducer(
 *   id = "update_book",
 *   name = @Translation("Update Book"),
 *   description = @Translation("Update an existing book entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Book")
 *   ),
 *   consumes = {
 *     "nid" = @ContextDefinition("string",
 *       label = @Translation("Book id")
 *     ),
 *     "data" = @ContextDefinition("any",
 *       label = @Translation("Book data")
 *     )
 *   }
 * )
 */
class UpdateBook extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The book manager service.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected BookManagerInterface $bookManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('book.manager'),
      $container->get('current_user')
    );
  }

  /**
   * CreateBook constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param array $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\book\BookManagerInterface $bookManager
   *   The book manager service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    BookManagerInterface $bookManager,
    AccountInterface $currentUser
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->bookManager = $bookManager;
    $this->currentUser = $currentUser;
  }

  /**
   * Updates an existing book.
   *
   * @param string|int|null $nid
   *   Node id of the book to look up.
   * 
   * @param array $data
   *   The submitted values for the book.
   * 
   * @return \Drupal\graphql_book\GraphQL\Response\BookResponse
   *   The updated book.
   * 
   * @throws \Exception
   */
  public function resolve(string $nid, array $data) {
    $response = new BookResponse();

    if (!$this->currentUser->hasPermission("Edit any content")) {
      $response->addViolation(
        $this->t('You do not have permissions to edit books.')
      );
      return $response;
    }

    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    if (!isset($node)) {
      $response->addViolation(
        $this->t('Could not find any book with the provided node ID.')
      );
      return $response;
    }
  
    $node->set('title', $data['title']);
    if (isset($data['body'])) {
      $node->set('body', $data['body']);
    }
    $node->save();

    if (isset($data['links'])) {
      $data['links']['nid'] = $nid;
      $this->bookManager->saveBookLink($data['links'], FALSE);
    }

    $response->setBook($node);
    return $response;
  }

}
