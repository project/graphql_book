<?php

namespace Drupal\graphql_book\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Load all IDs of entities matching provided arguments.
 *
 * @DataProducer(
 *   id = "load_entity_ids",
 *   name = @Translation("Load entity IDs"),
 *   description = @Translation("Load all IDs of entities matching provided arguments."),
 *   produces = @ContextDefinition("array",
 *     label = @Translation("Entity IDs")
 *   ),
 *   consumes = {
 *     "type" = @ContextDefinition("string",
 *       label = @Translation("Entity type")
 *     ),
 *     "bundles" = @ContextDefinition("string",
 *       label = @Translation("Entity bundle(s)"),
 *       multiple = TRUE,
 *       required = FALSE
 *     ),
 *     "check_access" = @ContextDefinition("boolean",
 *       label = @Translation("Check access"),
 *       required = FALSE,
 *       default_value = TRUE
 *     ),
 *   }
 * )
 */
class LoadEntityIDs extends DataProducerPluginBase implements ContainerFactoryPluginInterface {
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * EntityLoad constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Resolver.
   *
   * @param string $type
   * @param array|null $bundles
   * @param bool $checkAccess
   *
   * @return array
   * 
   * @throws \Exception
   */
  public function resolve($type, ?array $bundles, bool $checkAccess) {
    $query = $this->entityTypeManager->getStorage($type)->getQuery();

    if ($bundles) {
      $query = $query->condition('type', $bundles, 'IN');
    }

    return $query->accessCheck($checkAccess)->execute();
  }
}