<?php

namespace Drupal\graphql_book\Plugin\GraphQL\DataProducer;

use Drupal\book\BookManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Gets details about Book page links (parent pages and book top page), where applicable.
 *
 * @DataProducer(
 *   id = "load_book_links",
 *   name = @Translation("Load Book links"),
 *   description = @Translation("Gets details about Book page links (parent pages and book top page), where applicable."),
 *   produces = @ContextDefinition("array",
 *     label = @Translation("Book links")
 *   ),
 *   consumes = {
 *     "nid" = @ContextDefinition("string",
 *       label = @Translation("Book id")
 *     )
 *   }
 * )
 */
class LoadBookLinks extends DataProducerPluginBase implements ContainerFactoryPluginInterface {
  /**
   * The book manager service.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected BookManagerInterface $bookManager;

  /**
   * An array with already loaded book definitions.
   *
   * @var array
   */
  protected array $loadedBookLinks = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('book.manager')
    );
  }

  /**
   * GetBookDefinitions constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param array $pluginDefinition
   *   The plugin definition array.
   * @param \Drupal\book\BookManagerInterface $bookManager
   *   The book manager service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    BookManagerInterface $bookManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->bookManager = $bookManager;
  }

  /**
   * Loads and returns book links.
   *
   * @param string|int|null $nid
   *   Node id of the book to look up.
   *
   * @return array
   * 
   * @throws \Exception
   */
  public function resolve(string $nid) {
    $book = $this->bookManager->loadBookLink($nid, FALSE);

    if (!$book) {
      return [];
    }

    // Cache loaded book links
    if (!isset($this->loadedBookLinks[$nid])) {
      $this->loadedBookLinks[$nid] = $book;
    }

    return $this->loadedBookLinks[$nid];
  }
}