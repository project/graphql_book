<?php

namespace Drupal\graphql_book\Plugin\GraphQL\DataProducer;

use Drupal\book\BookManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\graphql_book\GraphQL\Response\BookResponse;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deletes a Book with the provided ID and returns the deleted entity.
 *
 * @DataProducer(
 *   id = "delete_book",
 *   name = @Translation("Delete Book"),
 *   description = @Translation("Deletes a Book with the provided ID and returns the deleted entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Book")
 *   ),
 *   consumes = {
 *     "nid" = @ContextDefinition("string",
 *       label = @Translation("Book ID")
 *     ),
 *   }
 * )
 */
class DeleteBook extends DataProducerPluginBase implements ContainerFactoryPluginInterface {
  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The book manager service.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected BookManagerInterface $bookManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('book.manager'),
      $container->get('current_user')
    );
  }

  /**
   * CreateArticle constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param array $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\book\BookManagerInterface $bookManager
   *   The book manager service.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    array $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    BookManagerInterface $bookManager,
    AccountInterface $currentUser
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->bookManager = $bookManager;
    $this->currentUser = $currentUser;
  }

  /**
   * Gets a book by ID and deletes it.
   *
   * @param string|int|null $nid
   *   ID of the book to delete.
   *
   * @return \Drupal\graphql_book\GraphQL\Response\BookResponse
   *   The book that was deleted as it was right before deletion.
   *
   * @throws \Exception
   */
  public function resolve(string $nid) {
    $response = new BookResponse();

    if (!$this->currentUser->hasPermission("Delete any content")) {
      $response->addViolation(
        $this->t('You do not have permissions to delete books.')
      );
      return $response;
    }

    $book = $this->entityTypeManager->getStorage('node')->load($nid);
    if (!isset($book)) {
      $response->addViolation(
        $this->t('Could not find any book with the provided node ID.')
      );
      return $response;
    }
    
    assert($book instanceof NodeInterface);

    if (!$this->bookManager->checkNodeIsRemovable($book)) {
      $response->addViolation(
        $this->t('Book cannot be removed because other content depends on it')
      );
      return $response;
    }

    $response->setBook($book);
    $book->delete();
    return $response;
  }
}