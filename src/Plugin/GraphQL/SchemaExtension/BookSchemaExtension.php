<?php

namespace Drupal\graphql_book\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\GraphQL\Response\ResponseInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\graphql_book\GraphQL\Response\BookResponse;

/**
 * @SchemaExtension(
 *   id = "book_extension",
 *   name = "Composable Book extension",
 *   description = "An extension that adds Book related fields.",
 *   schema = "book_schema"
 * )
 */
class BookSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('Query', 'books',
      $builder->compose(
        $builder->produce('load_entity_ids')
          ->map('type', $builder->fromValue('node'))
          ->map('bundles', $builder->fromValue(['book'])),
        $builder->produce('entity_load_multiple')
          ->map('type', $builder->fromValue('node'))
          ->map('bundles', $builder->fromValue(['book']))
          ->map('ids', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Query', 'book',
      $builder->produce('entity_load')
        ->map('type', $builder->fromValue('node'))
        ->map('bundles', $builder->fromValue(['book']))
        ->map('id', $builder->fromArgument('id'))
    );

    $registry->addFieldResolver('Mutation', 'createBook',
      $builder->compose(
        $builder->produce('create_book')
          ->map('data', $builder->fromArgument('data'))
      )
    );

    $registry->addFieldResolver('Mutation', 'updateBook',
      $builder->compose(
        $builder->produce('update_book')
          ->map('nid', $builder->fromArgument('id'))
          ->map('data', $builder->fromArgument('data'))
      )
    );

    $registry->addFieldResolver('Mutation', 'deleteBook',
      $builder->produce('delete_book')
        ->map('nid', $builder->fromArgument('id'))
    );

    $registry->addFieldResolver('BookResponse', 'book',
      $builder->callback(function (BookResponse $response) {
        return $response->book();
      })
    );

    $registry->addFieldResolver('BookResponse', 'errors',
      $builder->callback(function (BookResponse $response) {
        return $response->getViolations();
      })
    );

    $registry->addFieldResolver('Book', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver('Book', 'title',
      $builder->compose(
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver('Book', 'body',
      $builder->compose(
        $builder->produce('property_path')
          ->map('type', $builder->fromValue('entity:node'))
          ->map('value', $builder->fromParent())
          ->map('path', $builder->fromValue('body')),
        $builder->callback(function ($bodyArray) {
          $res = $bodyArray[0] ?? [
            'value' => '',
          ];
          return $res;
        })
      )
    );

    $registry->addFieldResolver('Book', 'links',
      $builder->compose(
        $builder->produce('entity_id')
          ->map('entity', $builder->fromParent()),
        $builder->produce('load_book_links')
          ->map('nid', $builder->fromParent())
      )
    );

    // Response type resolver.
    $registry->addTypeResolver('Response', [
      __CLASS__,
      'resolveResponse',
    ]);
  }

  /**
   * Resolves the response type.
   *
   * @param \Drupal\graphql\GraphQL\Response\ResponseInterface $response
   *   Response object.
   *
   * @return string
   *   Response type.
   *
   * @throws \Exception
   *   Invalid response type.
   */
  public static function resolveResponse(ResponseInterface $response): string {
    // Resolve content response.
    if ($response instanceof BookResponse) {
      return 'BookResponse';
    }
    throw new \Exception('Invalid response type.');
  }

}
