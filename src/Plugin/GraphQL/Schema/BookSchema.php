<?php

namespace Drupal\graphql_book\Plugin\GraphQL\Schema;

use Drupal\graphql\Plugin\GraphQL\Schema\ComposableSchema;

/**
 * @Schema(
 *   id = "book_schema",
 *   name = "Composable Book schema",
 *   extensions = "book_schema",
 * )
 */
class BookSchema extends ComposableSchema {

}
